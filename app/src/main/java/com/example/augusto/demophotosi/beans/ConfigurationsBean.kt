package com.example.augusto.demophotosi.beans

import com.google.gson.annotations.SerializedName

data class ConfigurationsBean(

    @SerializedName("category")
    val category: String = "",
    @SerializedName("categoryName")
    val categoryName: Any? = Any(),
    @SerializedName("correction")
    val correction: String = "",
    @SerializedName("correctionName")
    val correctionName: String = "",
    @SerializedName("creationDate")
    val creationDate: String = "",
    @SerializedName("currencyCode")
    val currencyCode: String = "",
    @SerializedName("endpoint")
    val endpoint: Any? = Any(),
    @SerializedName("format")
    val format: String = "",
    @SerializedName("formatName")
    val formatName: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("orientation")
    val orientation: String = "",
    @SerializedName("orientationName")
    val orientationName: String = "",
    @SerializedName("paper")
    val paper: String = "",
    @SerializedName("paperName")
    val paperName: String = "",
    @SerializedName("passepartout")
    val passepartout: String = "",
    @SerializedName("passepartoutName")
    val passepartoutName: String = "",
    @SerializedName("photoText")
    val photoText: String = "",
    @SerializedName("photoTextName")
    val photoTextName: String = "",
    @SerializedName("previews")
    val previews: List<Any> = listOf(),
    @SerializedName("productType")
    val productType: String = "",
    @SerializedName("publicUnitFormattedPrice")
    val publicUnitFormattedPrice: String = "",
    @SerializedName("publicUnitPrice")
    val publicUnitPrice: Int = 0,
    @SerializedName("quality")
    val quality: String = "",
    @SerializedName("qualityName")
    val qualityName: String = "",
    @SerializedName("quantity")
    val quantity: Int = 0,
    @SerializedName("updateDate")
    val updateDate: String = "",
    @SerializedName("uploaderConfigurationId")
    val uploaderConfigurationId: Int = 0,
    @SerializedName("userId")
    val userId: Int = 0
)