package com.example.augusto.demophotosi

import android.app.Application
import android.content.Context

class PhotosiApplication : Application() {


    companion object {

        private var instance : PhotosiApplication? = null
        val context: Context
            get() = instance!!.applicationContext
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }


}