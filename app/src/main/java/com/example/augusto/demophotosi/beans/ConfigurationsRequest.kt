package com.example.augusto.demophotosi.beans
import com.google.gson.annotations.SerializedName


data class ConfigurationsRequest(

    @SerializedName("category")
    var category: String = "",
    @SerializedName("correction")
    var correction: String = "",
    @SerializedName("format")
    var format: String = "",
    @SerializedName("paper")
    var paper: String = "",
    @SerializedName("passepartout")
    var passepartout: String = "",
    @SerializedName("photoText")
    var photoText: String = "",
    @SerializedName("quality")
    var quality: String = "",
    @SerializedName("quantity")
    var quantity: Int = 0
)