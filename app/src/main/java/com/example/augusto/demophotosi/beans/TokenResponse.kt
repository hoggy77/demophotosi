package com.example.augusto.demophotosi.beans

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TokenResponse(

    @SerializedName("token_type")
    @Expose
    val tokenType : String = "",
    @SerializedName("expires_in")
    @Expose
    val expiresIn : Int = 0,
    @SerializedName("access_token")
    @Expose
    val accessToken : String = "",
    @SerializedName("refresh_token")
    @Expose
    val refreshToken : String = ""
)