package com.example.augusto.demophotosi.beans
import com.google.gson.annotations.SerializedName


data class FileRequest(
    @SerializedName("configurationId")
    var configurationId: String = "",
    @SerializedName("quantity")
    var quantity: Int = 0,
    @SerializedName("type")
    var type: String = ""
)