package com.example.augusto.demophotosi.network

import android.text.TextUtils
import com.example.augusto.demophotosi.beans.TokenResponse
import com.example.augusto.demophotosi.constants.CredentialConstants
import com.example.augusto.demophotosi.extensions.addHeaders
import com.example.augusto.demophotosi.extensions.proceedWithRequest
import com.example.augusto.demophotosi.storage.PreferenceStoreManager
import com.google.gson.Gson
import okhttp3.FormBody
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.net.HttpURLConnection

/**
 * MainInterceptor è usato per intercettare le request delle chiamate di rete individua
 * -Token inesistente e ne crea uno nuovo se necessario
 * -Token scaduto ed effettua un refresh token
 * -Aggiunge headers alle chiamate
 */
class MainInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        var currentToken = PreferenceStoreManager.getInstance().getToken()
        val originalRequest = chain.request()

        /*
        Se il token non è presente
         */
        return if (TextUtils.isEmpty(currentToken)) {

            var formBody = FormBody.Builder()
                .add("client_id", CredentialConstants.CLIENT_ID)
                .add("client_secret", CredentialConstants.CLIENT_SECRET)
                .add("grant_type", CredentialConstants.GRANT_TYPE)
                .build()

            val newTokenRequest =
                originalRequest
                    .newBuilder()
                    .post(formBody)
                    .url(CredentialConstants.AUTH_URL)
                    .build()

            val refreshResponse = chain.proceedWithRequest(newTokenRequest)

            if (refreshResponse.isSuccessful) {
                val tokenResponse = Gson().fromJson(
                    refreshResponse.body()?.string(),
                    TokenResponse::class.java
                ) as TokenResponse
                PreferenceStoreManager.getInstance().setToken(tokenResponse.accessToken)
                PreferenceStoreManager.getInstance().setRefreshToken(tokenResponse.refreshToken)
                val newCall = originalRequest.newBuilder().addHeaders(tokenResponse.accessToken).build()
                chain.proceedWithRequest(newCall)

            } else chain.proceedWithRequest(chain.request().newBuilder().addHeaders(currentToken!!).build())

            /*
            Se il token è scaduto
             */
        } else if (chain.proceed(originalRequest).code() == HttpURLConnection.HTTP_UNAUTHORIZED) {

            var formBody = FormBody.Builder()
                .add(CredentialConstants.GRANT_TYPE_REFRESH, PreferenceStoreManager.getInstance().getRefresh())
                .add("grant_type", CredentialConstants.GRANT_TYPE_REFRESH)
                .build()

            val refreshTokenRequest =
                originalRequest
                    .newBuilder()
                    .post(formBody)
                    .url(CredentialConstants.AUTH_URL)
                    .build()
            val refreshResponse = chain.proceedWithRequest(refreshTokenRequest)

            if (refreshResponse.isSuccessful) {
                val refreshedToken = Gson().fromJson(
                    refreshResponse.body()?.string(),
                    TokenResponse::class.java
                ) as TokenResponse
                PreferenceStoreManager.getInstance().setToken(refreshedToken.accessToken)
                PreferenceStoreManager.getInstance().setRefreshToken(refreshedToken.refreshToken)
                val newCall = originalRequest.newBuilder().addHeaders(refreshedToken.accessToken).build()
                chain.proceedWithRequest(newCall)

            } else chain.proceedWithRequest(chain.request().newBuilder().addHeaders(currentToken!!).build())

            /*
            processamento request iniziale
             */
        } else chain.proceedWithRequest(originalRequest.newBuilder().addHeaders(currentToken!!).build())
    }


}
