package com.example.augusto.demophotosi.constants

class CredentialConstants{

    companion object {

        const val USERNAME = "e253d701ec@mailboxy.fun"
        const val PASSWORD = "111"
        const val CLIENT_ID = "f2bf330d607648fd3986eebca2addf"
        const val CLIENT_SECRET = "fa7bed010ae4f907daa095aa9e8eb1"
        //const val AUTH_URL = "https://auth.test.photosi.com/oauth2/token/?grant_type=client_credentials"
        const val AUTH_URL = "https://auth.test.photosi.com/oauth2/token"
        const val API_KEY = "AIzaSyAjcAUBXd7EA8cVryBH_XWDBUoLmZ6NWd4"
        const val USER_AGENT = "PRINTUP_ANDROID/9.9.9 (Software/PRINTUP_ANDROID; +http://corporate.photosi.com/)"
        const val GRANT_TYPE = "client_credentials"
        const val GRANT_TYPE_REFRESH = "refresh_token"
    }
}