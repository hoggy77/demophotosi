package com.example.augusto.demophotosi.network

import com.example.augusto.demophotosi.PhotosiApplication
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.create
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.*


class RetrofitClient {

    private val BASE_URL = "https://api.photoforse.online"
    private var retrofit: Retrofit? = null

    companion object {

        private var INSTANCE: RetrofitClient? = null

        fun getInstance(): RetrofitClient {
            if (INSTANCE == null) {
                INSTANCE = RetrofitClient()
            }

            return INSTANCE!!
        }

    }


    fun getClient(): Retrofit {

        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        return retrofit!!
    }

    private fun getOkHttpClient(): OkHttpClient {

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY


        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(20, TimeUnit.SECONDS)
            .addInterceptor(ChuckInterceptor(PhotosiApplication.context))
            .addInterceptor(MainInterceptor())
            .build()

    }


}