package com.example.augusto.demophotosi.beans

import com.google.gson.annotations.SerializedName

data class FileBean(

    @SerializedName("configurationId")
    val configurationId: String = "",
    @SerializedName("filename")
    val filename: Any? = Any(),
    @SerializedName("id")
    val id: String = "",
    @SerializedName("localFileUri")
    val localFileUri: Any? = Any(),
    @SerializedName("quantity")
    val quantity: Int = 0,
    @SerializedName("type")
    val type: String = "",
    @SerializedName("uploadUrl")
    val uploadUrl: String = "",
    @SerializedName("url")
    val url: String = ""
)