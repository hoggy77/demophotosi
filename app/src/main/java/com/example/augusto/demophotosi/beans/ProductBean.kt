package com.example.augusto.demophotosi.beans

import com.google.gson.annotations.SerializedName

data class ProductBean(

    @SerializedName("category")
    val category: String = "",
    @SerializedName("correction")
    val correction: String = "",
    @SerializedName("correctionName")
    val correctionName: String = "",
    @SerializedName("currencyCode")
    val currencyCode: String = "",
    @SerializedName("format")
    val format: String = "",
    @SerializedName("formatName")
    val formatName: String = "",
    @SerializedName("formattedPrice")
    val formattedPrice: String = "",
    @SerializedName("frontDatasheet")
    val frontDatasheet: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("orientation")
    val orientation: String = "",
    @SerializedName("orientationName")
    val orientationName: String = "",
    @SerializedName("paper")
    val paper: String = "",
    @SerializedName("paperName")
    val paperName: String = "",
    @SerializedName("passepartout")
    val passepartout: String = "",
    @SerializedName("passepartoutName")
    val passepartoutName: String = "",
    @SerializedName("photoText")
    val photoText: String = "",
    @SerializedName("photoTextName")
    val photoTextName: String = "",
    @SerializedName("previews")
    val previews: List<Any> = listOf(),
    @SerializedName("price")
    val price: Double = 0.0,
    @SerializedName("quality")
    val quality: String = "",
    @SerializedName("qualityName")
    val qualityName: String = ""
)