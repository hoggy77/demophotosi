package com.example.augusto.demophotosi.storage

import android.content.Context
import android.content.SharedPreferences
import com.example.augusto.demophotosi.PhotosiApplication
import android.R.id.edit



class PreferenceStoreManager {

    private val TOKEN_KEY = "token_key"
    private val REFRESH_TOKEN_KEY = "refresh_token_key"
    private val CONFIGURATOR_ID = "configurator_id"
    private val UPLOAD_URL = "upload_url"


    companion object {

        private val SHARED_NAME = "photosi_shared"

        private lateinit var mContext : Context
        private var INSTANCE : PreferenceStoreManager? = null
        private lateinit var preference : SharedPreferences

        fun getInstance() : PreferenceStoreManager{

            if (INSTANCE == null){
                preference = PhotosiApplication.context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)
                INSTANCE = PreferenceStoreManager()
            }

            return INSTANCE!!
        }
    }

    fun setToken(value : String){
        preference.edit().putString(TOKEN_KEY, value).commit()
    }

    fun getToken() : String?{
        return preference.getString(TOKEN_KEY, null)
    }

    fun setRefreshToken(value : String){
        preference.edit().putString(REFRESH_TOKEN_KEY, value).commit()
    }

    fun getRefresh() : String?{
        return preference.getString(REFRESH_TOKEN_KEY, null)
    }

    fun setConfiguratorID(value : String){

        preference.edit().putString(CONFIGURATOR_ID, value).commit()
    }

    fun getConfiguratorID() : String?{
        return preference.getString(CONFIGURATOR_ID, null)
    }


    fun setUploadUrl(value : String){
        preference.edit().putString(UPLOAD_URL, value).commit()
    }

    fun getUploadUrl() : String?{
        return preference.getString(UPLOAD_URL, null)
    }

}