@file:JvmName("NetworkUtils")
package com.example.augusto.demophotosi.extensions

import com.example.augusto.demophotosi.constants.CredentialConstants
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

fun Interceptor.Chain.proceedWithRequest(request: Request): Response {
    return proceed(request)

}

fun Request.Builder.addHeaders(token: String) =
    this.apply {
        header("Authorization", "Bearer $token")
        .header("User-Agent", CredentialConstants.USER_AGENT)
        .header("x-api-key", CredentialConstants.API_KEY)
    }
