package com.example.augusto.demophotosi.network;


import com.example.augusto.demophotosi.beans.*;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;
import java.util.Map;

public interface HttpServices {

  @GET("/configurators/consumer/prints/products")
    Single<List<ProductBean>> getProducts();

  @POST("/configurators/consumer/prints/configurations")
    Single<ConfigurationsBean> createConfiguration(@Body ConfigurationsRequest configurationsRequest);

  @POST("/configurators/consumer/prints/files")
  Single<FileBean> addConfiguration(@Body FileRequest fileRequest);

  @Multipart
  @PUT
  Single<ResponseBody> uploadFile(@Url String url, @Part MultipartBody.Part filePart);


}
