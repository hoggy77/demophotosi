package com.example.augusto.demophotosi.network

class NetworkProvider{

    companion object {

        fun getAPIService(): HttpServices {

            return RetrofitClient.getInstance().getClient().create(HttpServices::class.java)
        }

    }
}