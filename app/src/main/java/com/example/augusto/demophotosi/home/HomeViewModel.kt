package com.example.augusto.demophotosi.home

import android.content.Context
import android.net.wifi.hotspot2.pps.Credential
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.augusto.demophotosi.beans.*
import com.example.augusto.demophotosi.network.NetworkProvider
import com.example.augusto.demophotosi.storage.PreferenceStoreManager
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import java.math.MathContext
import java.util.*

class HomeViewModel : ViewModel() {

    private var disposable = CompositeDisposable()
    var configurationSubj: BehaviorSubject<Boolean> = BehaviorSubject.create()


    fun getProducts(): Single<List<ProductBean>> {

        return NetworkProvider.getAPIService().products
    }


    /**
     * EFFETTUO configurazione una sola volta e salvo Id e UploadUrl
     */
    fun getConfiguratorFlow() {

        var configurationRequest = ConfigurationsRequest()
        configurationRequest.let {
            it.category = "SMALL_SIZE"
            it.paper = "GLOSSY"
            it.passepartout = "NO"
            it.format = "10x10"
            it.quality = "NORMAL"
            it.correction = "AUTO"
            it.photoText = "NO"
            it.quantity = 1

            NetworkProvider.getAPIService().createConfiguration(configurationRequest)
                .flatMap { confBean ->

                    PreferenceStoreManager.getInstance().setConfiguratorID(confBean.id)

                    var fileRequest = FileRequest()
                    fileRequest.let {
                        it.configurationId = confBean.id
                        it.type = "SinglePageProductImage"
                        it.quantity = 1
                    }
                    NetworkProvider.getAPIService().addConfiguration(fileRequest)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ fileBean ->
                    PreferenceStoreManager.getInstance().setUploadUrl(fileBean.uploadUrl)
                    configurationSubj.onNext(true)
                }, {
                    configurationSubj.onNext(false)
                    Log.d("test", "errore durante add configuration...")
                })

        }
    }

    fun uploadFile(url: String, file: MultipartBody.Part): Single<ResponseBody> {

        return NetworkProvider.getAPIService().uploadFile(url, file)

    }


    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}