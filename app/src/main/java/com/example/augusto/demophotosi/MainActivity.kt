package com.example.augusto.demophotosi

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.augusto.demophotosi.home.HomeViewModel
import com.example.augusto.demophotosi.storage.PreferenceStoreManager
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.model.MediaFile
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import com.jaiselrahman.filepicker.config.Configurations
import id.zelory.compressor.Compressor
import okhttp3.MultipartBody
import java.text.DecimalFormat


class MainActivity : AppCompatActivity() {


    private lateinit var viewModel: HomeViewModel
    private val FILE_PICKER = 1001
    private var disposable = CompositeDisposable()
    private var selectedFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialize()
    }

    private fun initialize() {

        viewModel = ViewModelProviders.of(this@MainActivity).get(HomeViewModel::class.java)

        btnLogin.setOnClickListener {
            showPicker()
        }
    }


    /***
     * Apre gallery immagini per scelta singola della foto da inviare
     */
    private fun showPicker() {

        val intent = Intent(this, FilePickerActivity::class.java)
        intent.putExtra(
            FilePickerActivity.CONFIGS, Configurations.Builder()
                .setCheckPermission(true)
                .setShowImages(true)
                .setShowAudios(false)
                .setShowFiles(false)
                .setShowVideos(false)
                .enableImageCapture(false)
                .enableVideoCapture(false)
                .setSkipZeroSizeFiles(true)
                .setSingleChoiceMode(true)
                .build()
        )
        startActivityForResult(intent, FILE_PICKER)

    }


    /**
     * Applica una compressione all' immagine
     */
    private fun compressBeforeSend() {

        disposable.add(
            Compressor(this)
                .compressToFileAsFlowable(selectedFile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ compressedFile ->
                    Log.d("test", "Size compressed : %s".format(getReadableFileSize(compressedFile!!.length())))
                    uploadFile(compressedFile!!)
                    sizeCompressed.text = "Size compressed : %s".format(getReadableFileSize(compressedFile!!.length()))
                    setPreviewCompressed(compressedFile)

                }, {
                    Toast.makeText(this@MainActivity, getString(R.string.compress_file_error), Toast.LENGTH_LONG).show()
                })
        )

    }


    private fun getProducts() {

        disposable.add(
            viewModel.getProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    //ADAPTER
                }, {
                    //DIALOG TO SHOW ERROR
                })
        )

    }


    /**
     * Se non esiste crea una nuova configurazione e l' aggiunge
     */
    private fun createConfiguration() {

        disposable.add(
            viewModel.configurationSubj
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ configurationSuccess ->

                    if (configurationSuccess) {
                        compressBeforeSend()
                    } else {
                        Toast.makeText(
                            this@MainActivity,
                            getString(R.string.create_configuration_failed),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                },
                    {
                        Toast.makeText(
                            this@MainActivity,
                            getString(R.string.create_configuration_failed) + " " + it.message,
                            Toast.LENGTH_LONG
                        ).show()
                    })
        )

        viewModel.getConfiguratorFlow()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == FILE_PICKER && resultCode == Activity.RESULT_OK) {

            var files = data?.getParcelableArrayListExtra<MediaFile>(FilePickerActivity.MEDIA_FILES)
            if(files?.size!! > 0) {
                selectedFile = File(files!![0].path)
                sizeOriginal.text = "Size original : %s".format(getReadableFileSize(selectedFile!!.length()))
                setPreviewOriginal(selectedFile!!)
                Log.d("test", "Size original : %s".format(getReadableFileSize(selectedFile!!.length())))

                if (TextUtils.isEmpty(PreferenceStoreManager.getInstance().getConfiguratorID())) {

                    createConfiguration()
                } else {

                    compressBeforeSend()
                }

            }

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }


    /**
     *Effettuo l' upload del file
     */
    private fun uploadFile(file: File) {

        var filePart =
            MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MediaType.parse("image/*"), file))

        disposable.add(
            viewModel.uploadFile(PreferenceStoreManager.getInstance().getUploadUrl()!!, filePart)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    Toast.makeText(this@MainActivity, getString(R.string.file_upload_success), Toast.LENGTH_LONG)
                        .show()
                }, {

                    Toast.makeText(
                        this@MainActivity,
                        getString(R.string.file_upload_failed) + it.message,
                        Toast.LENGTH_LONG
                    ).show()
                })
        )


    }

    /**
     * Aggiorno l'immagine di preview nel caso di immagine originale
     */
    private fun setPreviewOriginal(fileOriginal: File) {
        containerNormal.visibility = View.VISIBLE
        var fileURI = Uri.fromFile(fileOriginal)
        Glide.with(this).load(fileURI).centerCrop().into(previewOriginal)
    }

    /**
     * Aggiorno l' immagine di preview nel caso di immagine compressa
     */
    private fun setPreviewCompressed(fileCompressed: File) {
        containerCompressed.visibility = View.VISIBLE
        var fileURI = Uri.fromFile(fileCompressed)
        Glide.with(this).load(fileURI).centerCrop().into(previewCompressed)
    }

    /**
     * Formattazione del size dei files
     */
    private fun getReadableFileSize(size: Long): String {
        if (size <= 0) {
            return "0"
        }
        val units = arrayOf("B", "KB", "MB", "GB", "TB")
        val digitGroups = (Math.log10(size.toDouble()) / Math.log10(1024.0)).toInt()
        return DecimalFormat("#,##0.#").format(
            size / Math.pow(1024.0, digitGroups.toDouble())
        ) + " " + units[digitGroups]
    }
}
